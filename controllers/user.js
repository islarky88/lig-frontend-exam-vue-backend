const { validationResult } = require("express-validator/check");
const cryptoRandomString = require("crypto-random-string");

const errorFormatter = ({ location, msg, param, value, nestedErrors }) => {
  // Build your resulting errors however you want! String, object, whatever - it works!
  // return `${location}[${param}]: ${msg}`
  return `${param}: ${msg}`;
};

const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const User = require("../models/user");

exports.login = async (req, res, next) => {
  const validate = validationResult(req).formatWith(errorFormatter);

  const username = req.body.username;
  const email = req.body.username;
  const password = req.body.password;
  const salt = cryptoRandomString(10);

  try {
    if (!validate.isEmpty()) {
      const error = new Error(validate.array());
      error.statusCode = 422;
      throw error;
    }

    // Checks if User is already registered or existing
    const user = await User.findOne({ where: { email: email } });

    // if user is not found, signup automatically and login
    if (!user) {
      const hashedPw = await bcrypt.hash(password + salt, 12);

      const user = new User({
        username: username,
        email: email,
        password: hashedPw,
        salt: salt
      });

      const result = await user.save();

      const token = jwt.sign(
        {
          email: user.email,
          userId: user.id
        },
        process.env.APP_JWT_SECRET,
        {
          expiresIn: process.env.APP_JWT_EXPIRY
        }
      );

      res.status(201).json({
        message: "User created!",
        token: token,
        userId: result.id
      });
      next();
    }

    const isEqual = await bcrypt.compare(password + user.salt, user.password);

    if (!isEqual) {
      const error = new Error("Email and Password does not match.");
      error.statusCode = 401;
      throw error;
    }

    const token = jwt.sign(
      {
        email: user.email,
        userId: user.id
      },
      process.env.APP_JWT_SECRET,
      {
        expiresIn: process.env.APP_JWT_EXPIRY
      }
    );
    res.status(200).json({
      message: "User Exist! Logging In.",
      token: token,
      userId: user.id
    });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};
