const fs = require("fs");
const path = require("path");
const slugify = require("slugify");

const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const { validationResult } = require("express-validator/check");

const io = require("../socket");
const User = require("../models/user");
const Messenger = require("../models/messenger");

// Send Message to messenger
exports.send = async (req, res, next) => {
  const user = await User.findOne({ where: { id: req.userId } });
  const senderId = user.id;
  const name = user.email.split("@")[0];
  const message = req.body.message;

  const messenger = new Messenger({
    senderId,
    name,
    message
  });

  try {
    const result = await messenger.save();
    // const user = await User.findById(req.userId)
    // user.posts.push(post)
    // await user.save()
    // io.getIO().emit('posts', {
    //   action: 'create',
    //   post: { ...post._doc, creator: { _id: req.userId, name: user.name } }
    // })
    res.status(201).json({
      message: "Message sent",
      data: result
    });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.getMessages = async (req, res, next) => {
  const msgsToGet = 20;

  try {
    const latestMessages = await Messenger.findAll({
      order: [["createdAt", "DESC"]],

      offset: (1 - 1) * msgsToGet,
      limit: msgsToGet,
      subQuery: false
    });

    // WHERE (e.info = `Raw` OR e.info = `Sub` OR e.info = `Info`) AND s.type = `Anime`

    if (!latestMessages) {
      const error = new Error("Could not find episode.");
      error.statusCode = 404;
      throw error;
    }

    res.status(200).json({
      data: latestMessages
    });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};
