const express = require("express");
const { body } = require("express-validator/check");

const userController = require("../controllers/user");

const router = express.Router();

router.post(
  "/login",
  [
    body("username")
      .trim()
      .isEmail()
      .withMessage("Please enter a valid email."),

    body("password")
      .trim()
      .isLength({ min: 5 })
      .withMessage("Password length should be at least 5 chars.")
  ],
  userController.login
);

module.exports = router;
