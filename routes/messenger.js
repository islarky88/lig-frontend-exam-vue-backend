const express = require("express");
const slugify = require("slugify");
const { body } = require("express-validator/check");

const User = require("../models/user");

const messengerController = require("../controllers/messenger");
const isAuth = require("../middleware/is-auth");

const router = express.Router();

// Send message to Messenger
router.post(
  "/send",
  isAuth,
  [
    body("message")
      .trim()
      .not()
      .isEmpty()
      .withMessage("Message should not be empty.")
  ],
  messengerController.send
);

router.post("/getMessages", isAuth, messengerController.getMessages);

module.exports = router;
