#!/usr/bin/env node
require("dotenv").config();

const express = require("express");
const bodyParser = require("body-parser");

const port = process.env.PORT || 3000;

// Routes

const userRoutes = require("./routes/user");
const messengerRoutes = require("./routes/messenger");

// Initialize Express
const app = express();

// app.use(bodyParser.urlencoded()); // x-www-form-urlencoded <form>
app.use(bodyParser.json()); // application/json

// CORS header
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "OPTIONS, GET, POST, PUT, PATCH, DELETE"
  );
  res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
  next();
});

// API Routes
app.use("/api/auth/", userRoutes);
app.use("/api/messenger/", messengerRoutes);

// API error catcher
app.use((error, req, res, next) => {
  console.log("ERRORS FOUND: " + error.message);
  const status = error.statusCode || 500;
  const message = error.message;
  const data = error.data;
  res.status(status).json({ error: message, data: data });
});

// start server
const server = app.listen(port);
